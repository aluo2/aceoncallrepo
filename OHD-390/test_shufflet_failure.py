from os import path

import onnx
import tvm
from tvm import auto_scheduler, relay
from tvm.relay import vm
from tvm.runtime import vm as vm_rt

# SRC: https://github.com/onnx/models/blob/main/vision/classification/densenet-121/README.md
MODEL_PATH = "OHD-390/shufflenet-9.onnx"


def ingest_passes(mod):
    passes = []

    # If the inputs are static, run DynamicToStatic to remove
    # any residual dynamism in the model.
    # If the inputs are dynamic, this pass is much more expensive
    # and will not remove dynamism from the model, so we skip it.
    passes.append(relay.transform.DynamicToStatic())

    # Infer types prior to the quantization pass below as some
    # transforms might need them.
    passes.append(relay.transform.InferType())

    # Transform fake quantized sub-graphs to actual integer ops.
    # Should have no effect on graphs without the relevant patterns.
    passes.append(relay.transform.FakeQuantizationToInteger())

    # Fold constants after FQ2I becuase some weights are stored in FP32.
    passes.append(relay.transform.FoldConstant())
    seq = tvm.transform.Sequential(passes)
    return seq(mod)


def nhwc_pass(mod):
    desired_layouts = {
        "nn.conv2d": ["NHWC", "default"],
        "nn.conv2d_transpose": ["NHWC", "default"],
        "nn.upsampling": ["NHWC", "default"],
        # "image.resize2d": ["NHWC", "default"], // TODO(mbrookhart): fix this in TVM
        "vision.roi_align": ["NHWC", "default"],
    }
    # We must use tvm.transform.Sequential since these passes have pre-req Passes
    # and Sequential guarantees we run those dependencies
    seq = tvm.transform.Sequential(
        [
            relay.transform.InferType(),
            relay.transform.ConvertLayout(desired_layouts),
            relay.transform.EliminateCommonSubexpr(),
            relay.transform.FoldConstant(),
        ]
    )
    with tvm.transform.PassContext(
        opt_level=3, config={"relay.backend.use_auto_scheduler": True}
    ):
        return seq(mod)


if __name__ == "__main__":
    onnx_model = onnx.load(MODEL_PATH)
    input_shapes = {}
    input_dtypes = {}
    initializer_names = [n.name for n in onnx_model.graph.initializer]

    # The inputs contains both the inputs and parameters. We are just interested in the
    # inputs so skip all parameters listed in graph.initializer
    for input_info in onnx_model.graph.input:
        if input_info.name not in initializer_names:
            name, shape, dtype, _ = relay.frontend.onnx.get_info(input_info)
            if dtype is None:
                raise ValueError(
                    f"Unknown dtype on input '{input_info.name}' is not supported."
                )
            input_shapes.update({input_info.name: shape})
            input_dtypes.update({input_info.name: dtype})

    mod, params = relay.frontend.from_onnx(
        onnx_model, shape=input_shapes, freeze_params=True
    )

    print("ingestion")
    mod = ingest_passes(mod)

    print("nhwc")
    mod = nhwc_pass(mod)

    print(mod)
    print("Compiling!")
    with auto_scheduler.ApplyHistoryBest("OHD-390/best_records.log"):
        with tvm.transform.PassContext(
            opt_level=3,
            config={
                "relay.backend.use_auto_scheduler": True,
                "relay.FuseOps.max_depth": 30,
            },
            disabled_pass=["tir.CommonSubexprElimTIR"],
        ):
            lib = relay.build_module.build(mod, target="llvm", params=params)
            """
            exe = vm.compile(
                mod,
                target="llvm",
                params=params,
            )
            """
    print("Compile success!")
