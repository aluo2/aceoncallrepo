import numpy as np
import onnx
import tvm
import tvm.utils
from tvm import autotvm, relay, target
from tvm.autotvm.tuner import XGBTuner
from tvm.relay import vm
from tvm.runtime import vm as vm_rt

MODEL_PATH = "OHD-445/resnet50-v1-12-int8.onnx"
SLOW_RECORDS = "OHD-445/slow_all_records.txt"
FAST_RECORDS = "OHD-445/fast_best_records.txt"
TARGET = target.Target("nvidia/geforce-rtx-3070")


def apply_relay_passes(
    mod: tvm.IRModule,
):
    """Applies relay passes to the input IRModule.

    :param mod: The input IRModule
    :return: The IRModule after all the relays passes have been applied
    """
    # N.B. Defer the import so as not to unconditionally require other runtimes.
    from tvm import relay, transform
    from tvm.relay.op.contrib.dnnl import rewrite_layer_norm

    mod = rewrite_layer_norm(mod)

    passes = []

    # If the inputs are static, run DynamicToStatic to remove
    # any residual dynamism in the model.
    # If the inputs are dynamic, this pass is much more expensive
    # and will not remove dynamism from the model, so we skip it.
    passes.append(relay.transform.DynamicToStatic())

    # Infer types prior to the quantization pass below as some
    # transforms might need them.
    passes.append(relay.transform.InferType())

    # Transform fake quantized sub-graphs to actual integer ops.
    # Should have no effect on graphs without the relevant patterns.
    passes.append(relay.transform.FakeQuantizationToInteger())

    passes.append(relay.transform.FastMath())

    # Fold constants after FQ2I becuase some weights are stored in FP32.
    passes.append(relay.transform.FoldConstant())

    # Use sequential to solve for dependent passes
    seq = transform.Sequential(passes)

    with tvm.transform.PassContext(opt_level=4):
        mod = seq(mod)

    mod = relay.transform.InferType()(mod)
    # mod["main"] = rewrite(relay.qnn.transform.LayerNormQuantizedRewrite(), mod["main"])

    return mod


def benchmark(mod, records, input_dict):
    saved_tir = tvm.utils.roofline.SaveLoweredTIR()
    with autotvm.apply_history_best(records):
        with tvm.transform.PassContext(
            opt_level=3,
            config={"relay.FuseOps.max_depth": 30},
            instruments=[saved_tir],
        ):
            exe = vm.compile(
                mod,
                TARGET,
                params={},
            )
            dev = tvm.device("cuda" if "cuda" in str(TARGET) else "cpu", 0)
            runtime = vm_rt.VirtualMachine(exe, dev)

    results = runtime.benchmark(
        tvm.cpu(),
        func_name="main",
        number=3,
        repeat=1,
        end_to_end=True,
        **input_dict,
    )  # End to end for being fair vs. onnxrt

    for global_var, tir in saved_tir.functions.items():
        print(global_var, "*" * 50)
        print(tir.script())
        print()
    print(results)


def load_records(record_file):
    with open(record_file, "r") as f:
        lines = f.readlines()
        return list(map(autotvm.record.decode, lines))


def get_model_mod():
    onnx_model = onnx.load(MODEL_PATH)
    mod, _ = relay.frontend.from_onnx(onnx_model, shape={"data": [1, 3, 224, 224]})
    mod = apply_relay_passes(mod)
    return mod


def benchmark_records(records, use_slow_records=True):
    records = SLOW_RECORDS if use_slow_records else FAST_RECORDS
    mod = get_model_mod()
    records = load_records(records)

    print("records loaded!")
    benchmark(
        mod,
        records,
        {"data": tvm.nd.array(np.zeros([1, 3, 224, 224]).astype("float32"))},
    )


def get_toy_model():
    data = relay.var("data", shape=[1, 64, 56, 56], dtype="uint8")
    # weight = relay.var("weight", shape=[64, 64, 1, 1], dtype="int8")
    weight = relay.const(np.random.randint(10, size=[64, 64, 1, 1], dtype="int8"))
    zero_point = relay.const(np.int32(0))
    scale = relay.const(np.float32(0.007))
    scale64 = relay.const(np.random.random(size=[64]).astype("float32"))
    zero_point64 = relay.const(np.zeros([64], dtype="int32"))
    conv = relay.qnn.op.conv2d(
        data,
        weight,
        input_zero_point=zero_point,
        kernel_zero_point=zero_point64,
        input_scale=scale,
        kernel_scale=scale64,
        kernel_size=(1, 1),
        channels=64,
        out_dtype="int32",
    )
    bias_add = relay.nn.bias_add(
        conv, relay.const(np.ones([64], dtype="int32")), axis=1
    )
    requantize = relay.qnn.op.requantize(
        bias_add, scale64, zero_point, scale, zero_point, axis=1, out_dtype="uint8"
    )
    mod = tvm.IRModule.from_expr(requantize)
    return mod


if __name__ == "__main__":
    mod = get_toy_model()
    mod = get_model_mod()
    print(mod)
    output_file = "output_file_slow"

    tasks = autotvm.task.extract_from_program(mod["main"], target=TARGET, params={})
    trials = 32
    for i, tsk in enumerate(reversed(tasks)):
        prefix = "[Task %2d/%2d] " % (i + 1, len(tasks))
        # do tuning
        tuner_obj = XGBTuner(tsk, loss_type="rank")
        tuner_obj.tune(
            n_trial=trials,
            measure_option=autotvm.measure_option(
                builder=autotvm.LocalBuilder(timeout=10),
                runner=autotvm.LocalRunner(
                    number=20, repeat=3, timeout=4, min_repeat_ms=150
                ),
            ),
            callbacks=[
                autotvm.callback.progress_bar(trials, prefix=prefix),
                autotvm.callback.log_to_file(output_file),
            ],
        )

    records = load_records(output_file)
    benchmark(
        mod,
        records,
        {
            "data": tvm.nd.array(
                # np.random.randint(10, size=[1, 64, 56, 56], dtype="uint8")
                np.random.random(size=[1, 3, 224, 224]).astype("float32")
            )
        },
    )
    
