import os
import os.path
import subprocess
import tempfile

import numpy as np
import onnx
import tensorflow as tf
import tvm
from onnx import shape_inference
from tvm.relay import vm
from tvm.runtime import vm as vm_rt

MODEL_PATH = "OHD-383/reader_US.tflite"
ONNX_MODEL = "OHD-383/reader_US.onnx"


def call_tf2onnx(model, opset: str) -> onnx.ModelProto:
    """Calls tf2onnx to convert tflite model to onnx.

    :param opset: ONNX opset
    :return: ONNX model bytes
    """
    # N.B. Defer the import so as not to unconditionally require other runtimes.
    import onnx

    result = subprocess.run(
        [
            "python",
            "-m",
            "tf2onnx.convert",
            "--tflite",
            MODEL_PATH,
            "--output",
            ONNX_MODEL,
            "--opset",
            opset,
        ],
        capture_output=True,
    )

    if "ERROR" in str(result.stderr):
        msg = "Exception converting to ONNX:\n"
        raise ValueError(msg + str(result.stderr))

    onnx_model = onnx.load_model(ONNX_MODEL)
    onnx_model = shape_inference.infer_shapes(onnx_model)
    onnx.save(onnx_model, ONNX_MODEL)
    return onnx_model


def to_relay(onnx_model, input_shapes):
    """Converts this Model to a RelayModel.

    :return: this Model converted to a RelayModel.
    """
    # N.B. Defer the import so as not to unconditionally require other runtimes.
    from tvm import relay

    mod, params = relay.frontend.from_onnx(
        onnx_model, shape=input_shapes, freeze_params=True
    )

    passes = []

    # If the inputs are static, run DynamicToStatic to remove
    # any residual dynamism in the model.
    # If the inputs are dynamic, this pass is much more expensive
    # and will not remove dynamism from the model, so we skip it.
    passes.append(relay.transform.DynamicToStatic())

    # Infer types prior to the quantization pass below as some
    # transforms might need them.
    passes.append(relay.transform.InferType())

    # Transform fake quantized sub-graphs to actual integer ops.
    # Should have no effect on graphs without the relevant patterns.
    passes.append(relay.transform.FakeQuantizationToInteger())

    # Fold constants after FQ2I becuase some weights are stored in FP32.
    passes.append(relay.transform.FoldConstant())

    # Use sequential to solve for dependent passes
    seq = tvm.transform.Sequential(passes)
    mod = seq(mod)
    return mod, params


num_threads = 1
interpreter = tf.lite.Interpreter(model_path=MODEL_PATH, num_threads=num_threads)

with open(MODEL_PATH, "rb") as f:
    model_bytes = f.read()

onnx_model = call_tf2onnx(model_bytes, "11")
relay_mod, relay_params = to_relay(onnx_model, {})
dev = tvm.device("llvm", 0)
host = tvm.cpu()
exe = vm.compile(relay_mod, "llvm", params=relay_params)
relay_vm = vm_rt.VirtualMachine(exe, dev)

inputs = {
    "normalized_input_image_tensor": tvm.nd.array(
        np.random.randint(0, 255, size=(1, 300, 300, 3)).astype("uint8")
    )
}
print(relay_mod)
"""
Regular run fails too
"""
relay_vm.run(**inputs)
breakpoint()
