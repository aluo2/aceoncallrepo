import onnx
import onnxruntime as ort

ONNX_MODEL = "OHD-383/reader_US.onnx"
import numpy as np

so = ort.SessionOptions()
so.log_severity_level = 3
so.enable_profiling = True
so.profile_file_prefix = "profiling_data"
session = ort.InferenceSession(ONNX_MODEL, sess_options=so)

inputs = {
    "normalized_input_image_tensor": np.random.randint(
        0, 255, size=(1, 300, 300, 3)
    ).astype("uint8")
}

result = session.run(None, inputs)

breakpoint()