import numpy as np
import tvm
from tvm import relay
from tvm.relay import vm
from tvm.runtime import vm as vm_rt

input = relay.var("input", shape=[relay.Any(), 3])

result = relay.strided_slice(
    input,
    begin=[1],
    end=[2],
    strides=[1],
    axes=[1],
)
result = relay.squeeze(result, axis=[1])

mod = tvm.IRModule.from_expr(result)
dev = tvm.device("llvm", 0)
host = tvm.cpu()
exe = vm.compile(mod, "llvm", params={})
relay_vm = vm_rt.VirtualMachine(exe, dev)

# size=[1,3], size=[0, 3] fails
result = relay_vm.run(np.random.uniform(0, 1, size=[1, 3]).astype("float32"))

print(result.numpy().shape)
