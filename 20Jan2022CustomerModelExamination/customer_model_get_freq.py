# Exploring the yaml file of customer models

import os
import pickle
import traceback
from collections import defaultdict

import google
import onnx
import onnx.helper
import tvm
import yaml
from async_timeout import sys
from tvm import relay

PATH_TO_CUSTOMER_YAML = (
    "/Users/andrewzhaoluo/Desktop/dev_tvm/aquarium/aquarium/data/customer_models.yaml"
)

"""Download models with something like 

gsutil -m cp -r gs://octoml-aquarium-customer-models/onnx/ 20Jan2022CustomerModelExamination/models/
"""
MODELS_FOLDER = "20Jan2022CustomerModelExamination/models/onnx"


def get_input_shapes(onnx_model):
    shapes = {}
    for input_tensor in onnx_model.graph.input:
        name = input_tensor.name
        dim_list = []
        shape = onnx_model.graph.input[0].type.tensor_type.shape.dim
        for i, n in enumerate(shape):
            v = n.dim_value
            if v == 0:
                if i == 0:
                    v = 1
                elif i == 1:
                    v = 3
                else:
                    v = 224
            dim_list.append(v)
        shapes[name] = dim_list
    return shapes


def get_analysis():
    with open(PATH_TO_CUSTOMER_YAML) as file:
        customer_model_entries = yaml.safe_load(file)

    # Config is a list with entries like:
    """
    {
        "name": "sightx-effdets0d0_nopost_op12_cf_pad_simp.onnx",
        "disabled": True,
        "customer_name": "SightX",
        "customer_model_name": "EffDetS0D0_NOPOST_op12_cf_PAD_simp.onnx",
        "format": "onnx",
        "model_uuid": "031326b5-0679-4bb6-a04c-f4f62ff14290",
        "model_variant_uuid": "714e87fe-1e1d-4b58-b931-fe7ecaf6122b",
        "dataref_uuid": "5d3a170f-9115-466f-b7de-99eaa39693a9",
        "gs_url": "https://storage.cloud.google.com/octoml-aquarium-customer-models/onnx/sightx-effdets0d0_nopost_op12_cf_pad_simp.onnx.onnx",
        "md5": "ff3376433dafb2faa4dfd2b674e954f4",
        "account_uuid": "02d1813b-fce7-4455-a476-ab31ecb7cfad",
        "input_shapes": [{"name": "x.1", "dtype": "float32", "shape": [1, 3, 512, 512]}],
    }
    """

    customer_model_entries = sorted(customer_model_entries, key=lambda a: a["name"])
    for i, entry in enumerate(customer_model_entries):
        name = entry["gs_url"].split("/")[-1]
        try:
            file_path = f"{MODELS_FOLDER}/{name}"
            input_shape_dicts = entry["input_shapes"]
            onnx_model = onnx.load(file_path)

            input_shapes = {}
            for shape_dict in input_shape_dicts:
                input_shapes[shape_dict["name"]] = shape_dict["shape"]

            mod, params = relay.frontend.from_onnx(
                onnx_model, shape=input_shapes, freeze_params=True
            )
            freq = relay.analysis.list_op_freqs(mod)
            entry["ops"] = freq
            print(f"Processed         {i + 1} / {len(customer_model_entries)}: {name}")
        except Exception:
            print(f"Failed processing {i + 1} / {len(customer_model_entries)}: {name}")

    pickle.dump(
        customer_model_entries,
        open("20Jan2022CustomerModelExamination/customer_model_op_freq.pkl", "wb"),
    )


def cleanup_analysis():
    result = pickle.load(
        open("20Jan2022CustomerModelExamination/customer_model_op_freq.pkl", "rb")
    )
    for entry in result:
        name = entry["gs_url"].split("/")[-1]
        if "mojo-vision" in name or "nota.ai" in name:
            continue

        if "ops" not in entry.keys():
            try:
                file_path = f"{MODELS_FOLDER}/{name}"
                input_shape_dicts = entry["input_shapes"]
                onnx_model = onnx.load(file_path)
                # original shape was incorrect, re-infer
                mod, params = relay.frontend.from_onnx(
                    onnx_model, shape=get_input_shapes(onnx_model), freeze_params=True
                )
                freq = relay.analysis.list_op_freqs(mod)
                entry["ops"] = freq
                print(name)
            except Exception:
                print(f"Failed {name}")
    pickle.dump(
        result,
        open("20Jan2022CustomerModelExamination/customer_model_op_freq.pkl", "wb"),
    )


def final_cleanup_analysis():
    result = pickle.load(
        open("20Jan2022CustomerModelExamination/customer_model_op_freq.pkl", "rb")
    )
    for entry in result:
        name = entry["gs_url"].split("/")[-1]
        if "ops" not in entry.keys():
            try:
                file_path = f"{MODELS_FOLDER}/{name}"
                onnx_model = onnx.load(file_path)
                # original shape was incorrect, re-infer
                mod, params = relay.frontend.from_onnx(onnx_model, freeze_params=True)
                freq = relay.analysis.list_op_freqs(mod)
                entry["ops"] = freq
                print(name)
            except Exception:
                print(f"Failed {name}")
    pickle.dump(
        result,
        open("20Jan2022CustomerModelExamination/customer_model_op_freq.pkl", "wb"),
    )


def print_top_k_operators(
    k=5, special_ops=["nn.conv1d", "nn.conv2d", "nn.conv3d", "nn.dense", "matmul"]
):
    result = pickle.load(
        open("20Jan2022CustomerModelExamination/customer_model_op_freq.pkl", "rb")
    )
    for entry in result:
        if "ops" not in entry.keys():
            continue

        print(entry["name"])
        ops = list(entry["ops"].items())
        ops = sorted(ops, key=lambda x: -x[1])
        for op_name, count in ops[:k]:
            print(f"\t{str(op_name):<20}: {int(count):>6}")

        top_k_ops = set([n[0] for n in ops[:k]])
        print(f"top-k special ops: {[s for s in special_ops if s in top_k_ops]}")
        print()


def print_get_freq_special_ops_only(
    special_ops=["nn.conv1d", "nn.conv2d", "nn.conv3d", "nn.dense", "matmul"]
):
    print(",".join(["name"] + special_ops + ["notes", "size (MB)"]))
    result = pickle.load(
        open("20Jan2022CustomerModelExamination/customer_model_op_freq.pkl", "rb")
    )
    for entry in result:
        if "ops" not in entry.keys():
            print(",".join([entry["name"]] + ["0"] * len(special_ops) + ["error", "0"]))
            continue

        # print(entry["name"])
        ops = list(entry["ops"].items())
        ops = sorted(ops, key=lambda x: -x[1])
        special_op_counts = defaultdict(int)
        for op_name, count in ops:
            if op_name in special_ops:
                special_op_counts[op_name] = count

        strings = [entry["name"]]
        for op in special_ops:
            strings.append(str(special_op_counts[op]))
        name = entry["gs_url"].split("/")[-1]
        file_path = f"{MODELS_FOLDER}/{name}"
        size = os.path.getsize(file_path) / 1000000
        strings.append("")
        strings.append(str(size))

        print(",".join(strings))


if __name__ == "__main__":
    # get_analysis()
    # cleanup_analysis()
    # final_cleanup_analysis()
    # print_top_k_operators(k=6)
    print_get_freq_special_ops_only()
