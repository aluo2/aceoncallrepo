import json

import numpy as np
import tensorflow as tf
import tvm
from tvm import relay
from tvm.contrib.debugger import debug_executor

"""Run debug graph runtime from saved relay json"""

_FUNC_NAME = "main"
_PARAMS_NAME = "__params__"

with open("relay_model.json") as f:
    json_obj = json.load(f)

for item in json_obj["nodes"]:
    if item["type_key"] == "IRModule":
        if item["attrs"].get("attrs") is None:
            item["attrs"].update({"attrs": "0"})
        break
model_json = json.dumps(json_obj)
module = tvm.ir.load_json(model_json)
params = module[_FUNC_NAME].attrs[_PARAMS_NAME]

mod = module
params = {k: v.data for k, v in params.items()}

inp = np.fromfile("input_ids.dat", dtype="int32")
inp = (np.reshape(inp, (1, 128))).astype(np.int32)

att = np.fromfile("attention_mask.dat", dtype="int32")
att = (np.reshape(att, (1, 128))).astype(np.int32)

ttids = np.fromfile("token_type_ids.raw", dtype="int32")
ttids = (np.reshape(ttids, (1, 128))).astype(np.int32)


shape_dict = {
    "input_ids": inp.shape,
    "attention_mask": att.shape,
    "token_type_ids": ttids.shape,
}
dtype_dict = {
    "input_ids": "int32",
    "attention_mask": "int32",
    "token_type_ids": "int32",
}

target = "llvm"


def run_tvm(lib):
    dev = tvm.device(target, 0)
    rt_mod = debug_executor.GraphModuleDebug(
        lib["debug_create"]("default", dev),
        [dev],
        lib.get_graph_json(),
        "./profiling_data_arm_m1_no_fuse/",
    )

    rt_mod.set_input("input_ids", inp)
    rt_mod.set_input("attention_mask", att)
    rt_mod.set_input("token_type_ids", ttids)
    rt_mod.run()
    tvm_res = rt_mod.get_output(0)
    return tvm_res, rt_mod


with tvm.transform.PassContext(opt_level=3, config={"relay.FuseOps.max_depth": 1}):
    lib = relay.build(mod, target=target, params=params)

tvm_res, rt_mod = run_tvm(lib)

print(tvm_res)
