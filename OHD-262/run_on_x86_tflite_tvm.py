import array
import json
import os

import numpy
import numpy as np
import onnx
import onnxruntime as ort
import tensorflow as tf
import tf2onnx
import tvm
import tvm.relay as relay
import tvm.relay.testing.tf as tf_testing
from tvm import te
from tvm.contrib import graph_executor
from tvm.contrib.debugger import debug_executor


def get_inputs(
    in_ids_file="OHD-262/input_ids/inp_ids_0.raw",
    att_mask_file="OHD-262/attention_mask/attn_mask_0.raw",
    token_type_file="OHD-262/token_type_ids.raw",
):
    inp = np.fromfile(in_ids_file, dtype="float32")
    inp = (np.reshape(inp, (1, 128))).astype(np.int32)

    att = np.fromfile(att_mask_file, dtype="float32")
    att = (np.reshape(att, (1, 128))).astype(np.int32)

    ttids = np.fromfile(token_type_file, dtype="int32")
    ttids = (np.reshape(ttids, (1, 128))).astype(np.int32)
    return inp, att, ttids


def run_tflite_model(
    input_ids,
    attention_mask,
    token_type_ids,
    model_file="OHD-262/bert-sst2-8w8a-layernorm-gelu-no-squared-difference.tflite",
):
    # Stop TFLite from flooding logs
    interpreter = tf.lite.Interpreter(model_path=model_file)
    interpreter.allocate_tensors()

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    print("Input Details = {}\n".format(input_details))
    print("Output Details = {}\n".format(output_details))

    interpreter.set_tensor(input_details[0]["index"], input_ids)
    interpreter.set_tensor(input_details[1]["index"], attention_mask)
    interpreter.set_tensor(input_details[2]["index"], token_type_ids)

    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    return output_data


def tflite_to_onnx(
    tflite_model="OHD-262/bert-sst2-8w8a-layernorm-gelu-no-squared-difference.tflite",
    onnx_output_file="OHD-262/bert-sst2-8w8a-layernorm-gelu-no-squared-difference-optimizations.onnx",
):
    onnx_graph = tf2onnx.tfonnx.process_tf_graph(
        None, opset=11, tflite_path=tflite_model
    )
    onnx_graph = tf2onnx.optimizer.optimize_graph(onnx_graph, catch_errors=True)
    onnx_model = onnx_graph.make_model("tf2onnx")
    onnx.save(
        onnx_model,
        onnx_output_file,
    )


def run_onnx_model(
    input_ids,
    attention_mask,
    token_type_ids,
    model_file="OHD-262/bert-sst2-8w8a-layernorm-gelu-no-squared-difference-optimizations.onnx",
):
    session = ort.InferenceSession(model_file)
    result = session.run(
        None,
        {
            "input_ids": input_ids,
            "attention_mask": attention_mask,
            "token_type_ids": token_type_ids,
        },
    )
    return result


def onnx_to_tvm_model(
    input_ids,
    attention_mask,
    token_type_ids,
    onnx_path="OHD-262/bert-sst2-8w8a-layernorm-gelu-no-squared-difference-optimizations.onnx",
    relay_model="OHD-262/bert-sst2-8w8a-layernorm-gelu-no-squared-difference-optimizations-relay.json",
):
    shape_dict = {
        "input_ids": input_ids.shape,
        "attention_mask": attention_mask.shape,
        "token_type_ids": token_type_ids.shape,
    }

    onnx_model = onnx.load(onnx_path)
    mod, _ = relay.frontend.from_onnx(onnx_model, shape_dict, freeze_params=True)
    mod = relay.transform.InferType()(mod)
    mod = relay.transform.FakeQuantizationToInteger()(mod)
    result = tvm.ir.save_json(mod)
    with open(relay_model, "w") as f:
        json.dump(result, f)


def run_tvm_model(
    input_ids,
    attention_mask,
    token_type_ids,
    relay_model="OHD-262/bert-sst2-8w8a-layernorm-gelu-no-squared-difference-optimizations-relay.json",
    debug_dump_dir="OHD-262/profiling_data_arm_m1_no_fuse/",
):
    with open(relay_model, "r") as f:
        json_obj = json.load(f)
    shape_dict = {
        "input_ids": input_ids.shape,
        "attention_mask": attention_mask.shape,
        "token_type_ids": token_type_ids.shape,
    }
    dtype_dict = {
        "input_ids": "int32",
        "attention_mask": "int32",
        "token_type_ids": "int32",
    }
    target = "llvm"

    mod = tvm.ir.load_json(json_obj)

    with tvm.transform.PassContext(opt_level=3, config={"relay.FuseOps.max_depth": 1}):
        lib = relay.build(mod, target=target, params={})

    dev = tvm.device(target, 0)

    rt_mod = debug_executor.GraphModuleDebug(
        lib["debug_create"]("default", dev),
        [dev],
        lib.get_graph_json(),
        debug_dump_dir,
    )

    rt_mod.set_input("input_ids", input_ids)
    rt_mod.set_input("attention_mask", attention_mask)
    rt_mod.set_input("token_type_ids", token_type_ids)
    rt_mod.run()
    tvm_res = rt_mod.get_output(0)
    return tvm_res


def examine_profile_data(
    base_dir="OHD-262/profiling_data_arm_m1_no_fuse/_tvmdbg_device_CPU_0",
):
    with open(f"{base_dir}/output_tensors.params", "rb") as f:
        results = dict(tvm.relay.load_param_dict(f.read()))
    with open(f"{base_dir}/_tvmdbg_graph_dump.json", "r") as f:
        graph = json.load(f)

    # To match up the stuff in the dump of params and the nodes in graph json
    def cleanup_tensor_names(l):
        return {"____".join(k.split("____")[:-1]): v for k, v in l.items()}

    results = cleanup_tensor_names(results)
    tensor_names = list(results.keys())

    # sort by topological index
    tensor_names = sorted(tensor_names, key=lambda x: int(x.split("topo-index:")[-1]))

    for i, (node_name, node) in enumerate(zip(tensor_names, graph["nodes"])):
        print(f"ARM: {node_name:<100}: \n\tInputs: {node['inputs']}")
        sample_arm = results[node_name].numpy().flatten()
        print(f"\t{sample_arm}")


if __name__ == "__main__":
    inp, att, ttids = get_inputs(
        in_ids_file=f"OHD-262/input_ids/inp_ids_22.raw",
        att_mask_file=f"OHD-262/attention_mask/attn_mask_22.raw",
    )
    tflite_result = run_tflite_model(inp, att, ttids)
    onnx_result = run_onnx_model(inp, att, ttids)
    tvm_result = run_tvm_model(inp, att, ttids)
    print(f"TFLITE : {tflite_result}")
    print(f"ONNX   : {onnx_result}")
    print(f"TVM   : {tvm_result}")  # This also dumps tensors
    examine_profile_data()
