import difflib
import json
import pickle

import tvm

with open(
    "profiling_data_arm_m1_no_fuse/_tvmdbg_device_CPU_0/output_tensors.params", "rb"
) as f:
    result_arm = dict(tvm.relay.load_param_dict(f.read()))

with open(
    "profiling_data_arm_m1_no_fuse/_tvmdbg_device_CPU_0/_tvmdbg_graph_dump.json", "r"
) as f:
    arm_graph = json.load(f)

# To match up the stuff in the dump of params and the nodes in graph json
def cleanup_tensor_names(l):
    return {"____".join(k.split("____")[:-1]): v for k, v in l.items()}


result_arm = cleanup_tensor_names(result_arm)
tensors_arm = list(result_arm.keys())
tensors_x86 = list(result_x86.keys())

results = []
for i, (arm_node, x86_node) in enumerate(zip(arm_graph["nodes"], x86_graph["nodes"])):
    arm_node_name = arm_node["name"]
    if arm_node_name.endswith("_"):
        arm_node_name = arm_node_name[:-1]

    x86_node_name = x86_node["name"]
    if x86_node_name.endswith("_"):
        x86_node_name = x86_node_name[:-1]

    arm_node_name += f"____topo-index:{i}"
    x86_node_name += f"____topo-index:{i}"

    print(i)
    print(f"ARM: {arm_node_name:<100}: \n\tInputs: {arm_node['inputs']}")
    sample_arm = result_arm[arm_node_name].numpy().flatten()
    print(f"\t{sample_arm}")

    print(f"x86: {x86_node_name:<100}: \n\tInputs: {x86_node['inputs']}")
    sample_x86 = result_x86[x86_node_name].numpy().flatten()
    print(f"\t{sample_x86}")
    print()

    results.append((sample_arm, sample_x86))
    import numpy as np

    if not np.allclose(sample_x86, sample_arm):
        breakpoint()


def different_tensor_investigation():
    # from x86 --> arm closest matches
    with open("name_mapping.pkl", "rb") as f:
        similar_name_map = pickle.load(f)

    def recreate_name_mapping_pkl():
        similar_name_map = {}
        for i, x86 in enumerate(tensors_x86):
            result = difflib.get_close_matches(x86, tensors_arm)
            print(f"*****{i} / {len(tensors_x86)}")
            print(x86)
            print(result[0])
            print(result)
            print()
            similar_name_map[x86] = result
        with open("name_mapping.pkl", "wb") as f:
            pickle.dump(similar_name_map, f)
        return similar_name_map

    different_results = []

    for x86 in tensors_x86:
        arm = similar_name_map[x86][0]

        print("*" * 30)
        print(arm)
        sample_arm = list(result_arm[arm].numpy().flatten())
        print(sample_arm)

        print(x86)
        sample_x86 = list(result_x86[x86].numpy().flatten())
        print(sample_x86)
        print()

        if sample_x86 != sample_arm:
            breakpoint()
            different_results.append((arm, x86))

    print("%" * 100)

    for arm, x86 in different_results:
        print("%" * 30)
        print(arm)
        print(result_arm[arm].numpy().flatten())
        print(x86)
        print(result_x86[x86].numpy().flatten())
